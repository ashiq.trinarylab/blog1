<?php

namespace App\Http\Controllers;

use App\Repositories\PostRepository;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct(public PostRepository $postRepository)
    {
    }

    public function index(Request $request): View
    {
        $posts = $this->postRepository->getLatestCachedPublicPosts(
            $request->get('cursor', 'first_page')
        );

        return view('home', compact('posts'));
    }
}
