<?php

namespace App\Http\Controllers;

use App\Events\PostsUpdated;
use App\Http\Requests\PostCreateRequest;
use App\Models\Post;
use App\Repositories\PostRepository;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct(public PostRepository $postRepository)
    {
    }

    public function index(Request $request): View
    {
        $order = $request->get('order') === 'asc' ? 'asc' : 'desc';
        $posts = $this->postRepository->getByUser($request->user(), $order);

        return view('posts.index', compact('posts'));
    }

    public function create(): View
    {
        return view('posts.create');
    }

    public function store(PostCreateRequest $request): RedirectResponse
    {
        Post::create(array_merge($request->validationData(), ['user_id' => auth()->id()]));
        PostsUpdated::dispatch();

        return redirect()->route('posts.index')->with('status', __('Post created successfully.'));
    }
}
