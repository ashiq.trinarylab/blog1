<?php

namespace App\Console\Commands;

use App\UpstreamBlogs\BlogImporter;
use Illuminate\Console\Command;

class ImportUpstreamBlogPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:upstream-blogs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import posts from upstream blogging platform';

    /**
     * Execute the console command.
     */
    public function handle(BlogImporter $blogImporter)
    {
        $blogImporter->import();
    }
}
