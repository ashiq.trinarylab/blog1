<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PostCacheInvalidated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
}
