<?php

namespace App\Providers;

use App\UpstreamBlogs\BlogImporter;
use Illuminate\Support\ServiceProvider;
use Psr\Log\LoggerInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->app->bind(BlogImporter::class, function () {
            return new BlogImporter(config('blog.upstream_blogging_platforms'), $this->app->make(LoggerInterface::class));
        });
    }
}
