<?php

namespace App\Repositories;

use App\Models\Post;
use App\Models\User;
use Illuminate\Contracts\Pagination\CursorPaginator;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cache;

class PostRepository
{
    public function getByUser(User $user, string $order = 'desc', int $perPage = 10): LengthAwarePaginator
    {
        return $user->posts()->orderBy('published_at', $order)->paginate($perPage)->withQueryString();
    }

    public function getLatestPublicPosts(int $perPage): CursorPaginator
    {
        return Post::with('user:id,name')
            ->select('title', 'description', 'published_at', 'user_id')
            ->latest('published_at')
            ->cursorPaginate($perPage);
    }

    public function getLatestCachedPublicPosts(string $cursor = 'first_page', int $perPage = 10): CursorPaginator
    {
        $cacheKey = Post::getPublicPageCacheKey($cursor);
        $cacheTagName = Post::getPublicPageCacheTagName();

        return Cache::tags([$cacheTagName])
            ->remember($cacheKey, now()->addDay(), function () use ($perPage) {
                return $this->getLatestPublicPosts($perPage);
            });
    }
}
