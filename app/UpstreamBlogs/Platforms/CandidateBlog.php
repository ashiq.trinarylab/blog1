<?php

namespace App\UpstreamBlogs\Platforms;

use App\Models\Post;
use App\Models\User;
use App\UpstreamBlogs\BloggingPlatform;
use App\UpstreamBlogs\Exceptions\UpstreamPlatformException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class CandidateBlog implements BloggingPlatform
{
    public function __construct(private readonly array $options)
    {
    }

    public function getName(): string
    {
        return $this->options['name'];
    }

    /**
     * {@inheritDoc}
     */
    public function fetch(): array
    {
        $response = Http::acceptJson()->get($this->options['url']);

        if (! ($response->successful() && $response->json('status') == 'ok')) {
            throw new UpstreamPlatformException($response->body(), $response->status(), customData: [
                'status' => $response->json(['status']),
            ]);
        }
        $admin = User::where('email', $this->options['user_email'])->first();

        return array_map(function ($item) use ($admin) {
            return new Post([
                'user_id' => $admin->id,
                'title' => $item['title'],
                'description' => $item['description'],
                'published_at' => Carbon::parse($item['publishedAt']),
            ]);
        }, $response->json('articles'));
    }
}
