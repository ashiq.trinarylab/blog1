<?php

namespace App\UpstreamBlogs\Exceptions;

use Exception;
use Throwable;

class UpstreamPlatformException extends Exception
{
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null, private readonly array $customData = [])
    {
        parent::__construct($message, $code, $previous);
    }

    public function getCustomData(): array
    {
        return $this->customData;
    }
}
