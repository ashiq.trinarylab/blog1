<?php

namespace App\UpstreamBlogs;

use App\Models\Post;
use App\UpstreamBlogs\Exceptions\UpstreamPlatformException;
use Illuminate\Http\Client\ConnectionException;

interface BloggingPlatform
{
    /**
     * @return Post[]
     *
     * @throws UpstreamPlatformException | ConnectionException
     */
    public function fetch(): array;

    public function getName(): string;
}
