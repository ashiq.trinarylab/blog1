<?php

namespace App\UpstreamBlogs;

use App\Events\PostsUpdated;
use App\UpstreamBlogs\Exceptions\UpstreamPlatformException;
use Exception;
use Illuminate\Support\Str;
use Psr\Log\LoggerInterface;

class BlogImporter
{
    public function __construct(private readonly array $platformConfigs, private readonly LoggerInterface $logger)
    {
    }

    public function import(): void
    {
        foreach ($this->platformConfigs as $platformConfig) {
            $this->importUsingPlatform($this->makePlatform($platformConfig));
        }
    }

    private function importUsingPlatform(BloggingPlatform $bloggingPlatform): void
    {
        try {
            $this->logger->info('fetching blog posts', [
                'platform' => $bloggingPlatform->getName(),
            ]);

            $posts = $bloggingPlatform->fetch();
            $this->storePosts($posts);

            $this->logger->info('fetch completed successfully', [
                'platform' => $bloggingPlatform->getName(),
                'post_found' => count($posts),
            ]);
        } catch (UpstreamPlatformException $upstreamPlatformException) {
            $this->logger->warning('fetch posts failed', [
                'platform' => $bloggingPlatform->getName(),
                'reason' => Str::limit($upstreamPlatformException->getMessage()),
                'http_code' => $upstreamPlatformException->getCode(),
                'custom_data' => $upstreamPlatformException->getCustomData(),
            ]);
        } catch (Exception $exception) {
            $this->logger->error('fetching posts failed', [
                'platform' => $bloggingPlatform->getName(),
                'reason' => Str::limit($exception->getMessage()),
            ]);
        }
    }

    private function makePlatform(array $platformConfig): BloggingPlatform
    {
        return app()->makeWith($platformConfig['fetcher'], [
            'options' => $platformConfig,
        ]);
    }

    private function storePosts(array $posts): void
    {
        foreach ($posts as $post) {
            $post->save();
        }
        PostsUpdated::dispatch();
    }
}
