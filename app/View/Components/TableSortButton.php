<?php

namespace App\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class TableSortButton extends Component
{
    public function iconClass(): string
    {
        $order = request()->get('order') == 'asc' ? 'asc' : 'desc';

        return "table-sort-column-$order-icon";
    }

    public function toUrl(): string
    {
        $order = request()->get('order', 'desc') == 'desc' ? 'asc' : 'desc';

        return url()->current().'?order='.$order;
    }

    public function render(): View
    {
        return view('components.table-sort-button');
    }
}
