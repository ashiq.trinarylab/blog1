<?php

namespace App\Listeners;

use App\Events\PostCacheInvalidated;
use App\Models\Post;
use Illuminate\Support\Facades\Cache;
use Psr\Log\LoggerInterface;

class PostCacheInvalidate
{
    public function __construct(private readonly LoggerInterface $logger)
    {

    }

    public function handle(): void
    {
        Cache::tags([Post::getPublicPageCacheTagName()])->flush();
        $this->logger->debug('post cache invalidated');

        PostCacheInvalidated::dispatch();
    }
}
