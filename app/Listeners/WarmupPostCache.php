<?php

namespace App\Listeners;

use App\Repositories\PostRepository;
use Psr\Log\LoggerInterface;

class WarmupPostCache
{
    public function __construct(private readonly LoggerInterface $logger, private readonly PostRepository $postRepository)
    {

    }

    public function handle(): void
    {
        $this->postRepository->getLatestCachedPublicPosts();
        $this->logger->debug('post cache warmed');
    }
}
