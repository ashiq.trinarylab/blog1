<?php

use App\UpstreamBlogs\Platforms\CandidateBlog;

return [
    'system_admin_password' => env('SYSTEM_ADMIN_PASSWORD', 'password'),
    'upstream_blogging_platforms' => [
        [
            'user_email' => 'admin@demo.com',
            'name' => 'candidate blog',
            'url' => 'https://candidate-test.sq1.io/api.php',
            'fetcher' => CandidateBlog::class,
        ],
    ],
];
