<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SystemAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::updateOrCreate([
            'email' => 'admin@demo.com',
        ], [
            'name' => 'admin',
            'password' => Hash::make(config('blog.system_admin_password')),
            'email_verified_at' => now(),
        ]);
    }
}
