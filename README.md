# Blog1

Blog1 is a blogging platform where anonymous users can read posts posted by registered users. Anyone can register and log in to the system to create posts.

###  Requirements
* Docker version >= 20.0

## Installation Guide

1. Clone repository
```bash
git clone https://gitlab.com/ashiq.trinarylab/blog1.git
```
2. Go to project dir
```bash
cd blog1
```
3. Setup `.env` file
```bash
cp .env.example .env
```
4. Install dependencies using `sail`
```base
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php82-composer:latest \
    composer install --ignore-platform-reqs
```
5. Run application using `sail`
```bash
./vendor/bin/sail up -d
```
6. Build, migrate and seed
```bash
./vendor/bin/sail npm i
./vendor/bin/sail npm run build
./vendor/bin/sail artisan key:generate
./vendor/bin/sail artisan migrate
./vendor/bin/sail artisan db:seed
```
7. Go to [Blog1](http://localhost:8000) to view site.


## How to import upstream blogging platform posts

To import post manually from blogging platforms run following command
```bash
./vendor/bin/sail artisan import:upstream-blogs
```

**For production Deployment:**

To import blog posts periodically, add Laravel's scheduler command in cron entry.
```base
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```
It will pull posts every 10 minutes.

## Run tests

```bash
./vendor/bin/sail test
```

