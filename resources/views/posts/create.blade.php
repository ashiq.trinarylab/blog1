<x-app-layout>
    <div class="min-h-min flex flex-col sm:justify-center items-center sm:pt-0 bg-gray-100 mt-2">
        <div class="w-full sm:max-w-3xl mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
            <div class="p-6 text-gray-900">
                <form method="POST" action="{{ route('posts.store') }}">
                    @csrf

                    <div>
                        <x-input-label for="title" :value="__('Title')"/>
                        <x-text-input id="title" class="block mt-1 w-full" type="text" name="title"
                                      :value="old('title')" required autofocus/>
                        <x-input-error :messages="$errors->get('title')" class="mt-2"/>
                    </div>

                    <div class="mt-4">
                        <x-input-label for="description" :value="__('Description')"/>
                        <x-text-area-input id="description" class="block mt-1 w-full"
                                           name="description" rows="6" required>
                            {{old('description')}}
                        </x-text-area-input>
                        <x-input-error :messages="$errors->get('description')" class="mt-2"/>
                    </div>
                    <div class="mt-4">
                        <x-input-label for="published_at" :value="__('Publication Date')"/>
                        <x-text-input id="published_at" class="block mt-1 w-full" type="datetime-local" name="published_at"
                                      :value="old('published_at')" required autofocus/>
                        <x-input-error :messages="$errors->get('published_at')" class="mt-2"/>
                    </div>

                    <div class="flex items-center justify-end mt-4">
                        <x-primary-button class="ml-3">
                            {{ __('Create') }}
                        </x-primary-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
