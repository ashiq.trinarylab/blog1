<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <x-session-status class="mb-4 text-2xl" :status="session('status')" />
                    <div class="mt-2 mb-6 flex justify-end">
                        <a href="{{route('posts.create')}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Create
                            Post</a>
                    </div>
                    <div class="overflow-x-auto">
                        @if($posts->isEmpty())
                        <div class="flex justify-center text-2xl pb-6">
                            {{ _('You haven\'t created any posts yet!')}}
                        </div>
                        @else
                        <table class="min-w-full border-collapse border border-gray-300">
                            <thead>
                            <tr>
                                <th class="p-3 border border-gray-300">ID</th>
                                <th class="p-3 border border-gray-300">Title</th>
                                <th class="p-3 border border-gray-300">Description</th>
                                <th class="p-3 border border-gray-300 min-w-[170px]">
                                    <x-table-sort-button>Publication Date</x-table-sort-button>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                            <tr>
                                <td class="p-3 border border-gray-300">{{ $posts->perPage() * ($posts->currentPage() -
                                    1) + $loop->iteration }}
                                </td>
                                <td class="p-3 border border-gray-300">{{$post->title}}</td>
                                <td class="p-3 border border-gray-300">{{$post->description}}</td>
                                <td class="p-3 border border-gray-300">{{$post->published_at->toDayDateTimeString()}}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="mt-4">
                            {{$posts->links()}}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
