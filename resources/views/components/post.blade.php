<div class="border-2 mb-4 p-6">
    <h1 class="text-gray-900 text-2xl font-bold">{{$post->title}}</h1>
    <p>{{$post->user->name}} | {{$post->published_at->toDayDateTimeString()}}</p>
    <p class="mt-2">
        {{$post->description}}
    </p>
</div>
