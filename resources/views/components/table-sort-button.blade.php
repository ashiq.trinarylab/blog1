<div class="flex gap-1 cursor-pointer" onclick="window.location='{{$toUrl()}}'">
    <span class="mr-1">{{ $slot }}</span>
    <i class="mt-2 {{ $iconClass()}}"></i>
</div>
