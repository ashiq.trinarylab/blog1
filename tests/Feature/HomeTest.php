<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    public function test_home_page_is_displayed_by_guest(): void
    {
        $response = $this->get('/');

        $response->assertOk();
    }

    public function test_home_page_show_login_and_register_for_guest(): void
    {
        $response = $this->get('/');

        $response->assertSee('Login');
        $response->assertSee('Register');
    }

    public function test_home_page_is_displayed_for_authenticated_user(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/');

        $response->assertOk();
    }

    public function test_home_page_show_my_posts_all_posts_and_logout_menu_for_authenticated_user(): void
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/');

        $response->assertSee('All Posts');
        $response->assertSee('My Posts');
        $response->assertSee('Log Out');
        $response->assertSee($user->name);
    }

    public function test_home_page_shows_latest_posts_ordered_by_publication_date(): void
    {
        $posts = Post::factory(10)->create()->sortByDesc('published_at')->values();
        $response = $this->get('/');
        $response->assertSeeTextInOrder([
            $posts[0]->title, $posts[0]->published_at->toDayDateTimeString(),
            $posts[1]->title, $posts[1]->published_at->toDayDateTimeString(),
            $posts[2]->title, $posts[2]->published_at->toDayDateTimeString(),
        ]);
    }

    public function test_home_page_has_cursor_pagination(): void
    {
        $posts = Post::factory(19)->create()->sortByDesc('published_at')->values();
        $response = $this->get('/');

        $response->assertSee($posts[9]->title);
        $response->assertDontSee($posts[10]->title);

        preg_match_all('/(cursor=[\w]+)"/m', $response->getContent(), $matches, PREG_SET_ORDER);
        $this->assertCount(1, $matches);

        $response = $this->get('/?'.$matches[0][1]);
        $response->assertDontSee($posts[9]->title);
        $response->assertSee($posts[10]->title);
        $response->assertSee($posts[18]->title);
    }
}
