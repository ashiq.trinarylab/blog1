<?php

namespace Tests\Feature;

use App\Events\PostsUpdated;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    public function test_unauthenticated_user_redirected_to_login_if_tried_to_access_my_posts(): void
    {
        $response = $this->get('/posts');

        $response->assertRedirectToRoute('login');
    }

    public function test_my_posts_shows_only_posts_those_published_by_authenticated_user(): void
    {
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();

        $user1Posts = Post::factory(3)->create(['user_id' => $user1->id])->sortByDesc('published_at')->values();
        $user2Posts = Post::factory(4)->create(['user_id' => $user2->id])->sortByDesc('published_at')->values();

        $response = $this->actingAs($user1)->get('/posts');
        $response->assertSeeTextInOrder([
            $user1Posts[0]->title,
            $user1Posts[2]->title,
        ]);
        $response->assertDontSee([
            $user2Posts[0]->title,
        ]);

        $response = $this->actingAs($user2)->get('/posts');
        $response->assertSee([
            $user2Posts[0]->title,
        ]);
    }

    public function test_my_posts_can_be_ordered_descending_by_publication_date(): void
    {
        $user = User::factory()->create();
        $posts = Post::factory(3)->create(['user_id' => $user->id])->sortByDesc('published_at')->values();

        $response = $this->actingAs($user)->get('/posts?order=desc');
        $response->assertSeeTextInOrder([
            $posts[0]->title,
            $posts[2]->title,
        ]);
    }

    public function test_my_posts_can_be_ordered_ascending_by_publication_date(): void
    {
        $user = User::factory()->create();
        $posts = Post::factory(3)->create(['user_id' => $user->id])->sortBy('published_at')->values();

        $response = $this->actingAs($user)->get('/posts?order=asc');
        $response->assertSeeTextInOrder([
            $posts[0]->title,
            $posts[2]->title,
        ]);
    }

    public function test_my_post_has_pagination(): void
    {
        $user = User::factory()->create();
        $posts = Post::factory(19)->create(['user_id' => $user->id])->sortByDesc('published_at')->values();

        $response = $this->actingAs($user)->get('/posts?page=2');
        $response->assertSeeTextInOrder([
            $posts->get(10)->title,
            $posts->get(18)->title,
        ]);
    }

    public function test_shows_post_creation_form()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/posts/create');

        $response->assertSeeText([
            'Title',
            'Description',
            'Publication Date',
            'Create',
        ]);
    }

    public function test_post_creations_form_submit_show_validation_errors_for_invalid_data()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post('/posts', []);

        $response->assertSessionHasErrors([
            'title',
            'description',
            'published_at',
        ]);
    }

    public function test_post_create_if_data_is_valid()
    {
        $user = User::factory()->create();

        [$title, $description, $publishedAt] = ['sample title', 'sample description', now()];
        $response = $this->actingAs($user)->post('/posts', [
            'title' => $title,
            'description' => $description,
            'published_at' => $publishedAt,
        ]);

        $response->assertRedirect('/posts')
            ->assertSessionHasNoErrors()
            ->assertSessionHas('status', __('Post created successfully.'));

        $this->assertDatabaseHas('posts', [
            'title' => $title,
            'description' => $description,
            'published_at' => $publishedAt,
        ]);
    }

    public function test_posts_update_event_fired_if_post_is_created()
    {
        $user = User::factory()->create();
        Event::fake();

        [$title, $description, $publishedAt] = ['sample title', 'sample description', now()];
        $this->actingAs($user)->post('/posts', [
            'title' => $title,
            'description' => $description,
            'published_at' => $publishedAt,
        ]);

        Event::assertDispatched(PostsUpdated::class);
    }

    public function test_posts_cache_invalidated_and_warmed_after_new_post_created()
    {
        $user = User::factory()->create();

        $postsData = [
            [
                'title' => 'title 1',
                'description' => 'description 1',
                'published_at' => now()->subDay(),
            ],
            [
                'title' => 'title 2',
                'description' => 'description 2',
                'published_at' => now()->subHour(),
            ],
        ];

        $response = $this->get('/');
        $response->assertDontSee('title 1');
        $response->assertDontSee('title 2');

        $this->actingAs($user)->post('/posts', $postsData[0]);
        $response = $this->get('/');

        $response->assertSee('title 1');
        $response->assertDontSee('title 2');

        $this->actingAs($user)->post('/posts', $postsData[1]);
        $response = $this->get('/');

        $response->assertSeeTextInOrder([
            'title 2',
            'title 1',
        ]);
    }
}
