<?php

namespace Tests\Feature;

use App\Events\PostsUpdated;
use App\Models\Post;
use App\Models\User;
use App\UpstreamBlogs\BloggingPlatform;
use App\UpstreamBlogs\BlogImporter;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Psr\Log\LoggerInterface;
use Tests\TestCase;

class BlogImporterTest extends TestCase
{
    use RefreshDatabase;

    private LoggerInterface $logger;

    private array $platformConfigs;

    private BloggingPlatform $platformMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->logger = $this->mock(LoggerInterface::class)->shouldIgnoreMissing();
        $this->platformMock = $this->mock(BloggingPlatform::class)->shouldIgnoreMissing();
        $this->platformConfigs = [
            [
                'user_email' => 'admin@demo.com',
                'name' => 'blog1',
                'url' => 'https://example-blog1.com',
                'fetcher' => 'Platform1',
            ],
        ];
        $this->app->bind('Platform1', function ($app, $parameters) {
            return $this->platformMock;
        });
    }

    public function test_blog_platform_fetcher_called_with_correct_config(): void
    {
        $this->app->bind('Platform1', function ($app, $parameters) {
            $this->assertEquals($parameters, [
                'options' => $this->platformConfigs[0],
            ]);

            return $this->platformMock;
        });

        $importer = new BlogImporter($this->platformConfigs, $this->logger);

        $importer->import();
    }

    public function test_posts_store_if_platform_return_posts()
    {
        $admin = User::factory()->create();

        $postData = [
            'user_id' => $admin->id,
            'title' => 'title 1',
            'description' => 'description 1',
            'published_at' => Carbon::now()->subHour(),
        ];
        $this->platformMock->shouldReceive('fetch')->once()->andReturn([new Post($postData)]);

        $importer = new BlogImporter($this->platformConfigs, $this->logger);
        $importer->import();
        $this->assertDatabaseHas('posts', $postData);
    }

    public function test_posts_updated_events_fired_once_after_import_completed()
    {
        $admin = User::factory()->create();

        Event::fake();

        $postData = [
            'user_id' => $admin->id,
            'title' => 'title 1',
            'description' => 'description 1',
            'published_at' => Carbon::now()->subHour(),
        ];
        $this->platformMock->shouldReceive('fetch')->once()->andReturn([new Post($postData)]);
        $importer = new BlogImporter($this->platformConfigs, $this->logger);
        $importer->import();

        Event::assertDispatched(PostsUpdated::class);
    }

    public function test_posts_cache_invalidated_and_warmed_after_importing_platform_posts()
    {
        $admin = User::factory()->create();
        $postData = [
            'user_id' => $admin->id,
            'title' => 'title 1',
            'description' => 'description 1',
            'published_at' => Carbon::now()->subHour(),
        ];

        $response = $this->get('/');
        $response->assertDontSee('title 1');

        $this->platformMock->shouldReceive('fetch')->andReturn([new Post($postData)]);
        $importer = new BlogImporter($this->platformConfigs, $this->logger);
        $importer->import();

        $response = $this->get('/');
        $response->assertSee('title 1');
        $response->assertSee($admin->name);
    }
}
