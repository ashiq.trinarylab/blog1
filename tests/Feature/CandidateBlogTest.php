<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use App\UpstreamBlogs\Exceptions\UpstreamPlatformException;
use App\UpstreamBlogs\Platforms\CandidateBlog;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class CandidateBlogTest extends TestCase
{
    use RefreshDatabase;

    public function test_return_post_model_for_each_articles_entry_in_response_for_successful_response(): void
    {
        $responseRaw = [
            'status' => 'ok',
            'count' => 3,
            'articles' => [
                [
                    'id' => 3,
                    'title' => 'title for id 3',
                    'description' => 'description for id 3',
                    'publishedAt' => '2022-08-31T10:31:39Z',
                ],
                [
                    'id' => 2,
                    'title' => 'title for id 2',
                    'description' => 'description for id 2',
                    'publishedAt' => '2022-08-31T10:37:42Z',
                ],
                [
                    'id' => 5,
                    'title' => 'title for id 5',
                    'description' => 'description for id 5',
                    'publishedAt' => '2022-08-31T10:23:00Z',
                ],
            ],
        ];

        $platformConfig = [
            'user_email' => 'admin@demo.com',
            'name' => 'candidate blog',
            'url' => 'http://example.com',
            'fetcher' => CandidateBlog::class,
        ];

        $admin = User::factory()->create([
            'email' => $platformConfig['user_email'],
        ]);

        Http::fake([
            $platformConfig['url'] => Http::response($responseRaw),
        ]);

        $candidateBlog = new CandidateBlog($platformConfig);

        $posts = $candidateBlog->fetch();

        $this->assertCount(3, $posts);
        foreach ($posts as $post) {
            $this->assertInstanceOf(Post::class, $post);
        }

        $expectedItems = array_map(function ($item) use ($admin) {
            $item['user_id'] = $admin->id;
            $item['published_at'] = Carbon::parse($item['publishedAt'])->toDayDateTimeString();
            unset($item['id']);
            unset($item['publishedAt']);

            return $item;
        }, $responseRaw['articles']);

        $gotItems = array_map(function (Post $post) {
            return [
                'user_id' => $post->user_id,
                'title' => $post->title,
                'description' => $post->description,
                'published_at' => $post->published_at->toDayDateTimeString(),
            ];
        }, $posts);

        $this->assertEqualsCanonicalizing($expectedItems, $gotItems);
    }

    public function test_throw_upstream_platform_exception_if_response_status_not_ok(): void
    {
        $platformConfig = [
            'user_email' => 'admin@demo.com',
            'name' => 'candidate blog',
            'url' => 'http://example.com',
            'fetcher' => CandidateBlog::class,
        ];

        $responseRaw = [
            'status' => 'error',
        ];

        Http::fake([
            $platformConfig['url'] => Http::response($responseRaw),
        ]);

        $candidateBlog = new CandidateBlog($platformConfig);
        $this->expectExceptionCode(200);
        $this->expectException(UpstreamPlatformException::class);
        $candidateBlog->fetch();
    }

    public function test_throw_upstream_platform_exception_if_request_is_not_successful(): void
    {
        $platformConfig = [
            'user_email' => 'admin@demo.com',
            'name' => 'candidate blog',
            'url' => 'http://example.com',
            'fetcher' => CandidateBlog::class,
        ];

        $responseRaw = [
            'status' => 'ok',
        ];

        Http::fake([
            $platformConfig['url'] => Http::response($responseRaw, 400),
        ]);

        $candidateBlog = new CandidateBlog($platformConfig);
        $this->expectException(UpstreamPlatformException::class);
        $this->expectExceptionCode(400);
        $candidateBlog->fetch();
    }
}
